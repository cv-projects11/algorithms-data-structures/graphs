package algorithms.challenges

//from random coding interview(i think?)
//given list of positive integers, "stock prices" find the best day to buy and sell
//max{arr[j] - arr[i]}, i < j

fun findBestDaysToBuy(prices: IntArray): Pair<Int, Int> {

    when (prices.size) {
        1 -> error("unspecified")
        2 -> return 0 to 1
    }

    var currentMin = prices[0]
    var currentMinIndex = 0
    var currentBest = 0
    var currentBestIndexes = 0 to 1
    for (i in 1 until prices.size) {
        val currentValue = prices[i]
        if (currentValue < currentMin) {
            currentMin = currentValue
            currentMinIndex = i
        } else if (profit(currentMin, currentValue) > currentBest) {
            currentBest = profit(currentMin, currentValue)
            currentBestIndexes = currentMinIndex to i
        }
    }

    return currentBestIndexes
}

private fun profit(buy: Int, sell: Int) = sell - buy
