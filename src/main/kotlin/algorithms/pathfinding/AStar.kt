package algorithms.pathfinding

import collections.Edge
import collections.Graph
import collections.Node
import collections.forEachEdgeOfNode
import java.util.*
import kotlin.math.min

fun <T> aStar(graph: Graph<T>, src: Node<T>, dest: Node<T>, heuristic: (Node<T>) -> Double = { 0.0 }): List<Edge<T>> {
    val distances: MutableMap<Node<T>, Double> = distanceInit(graph, src)
    val pqNodes = PriorityQueue<Node<T>>(graph.nodeCount,
        Comparator.comparing { distances[it]!! + heuristic(it) }).apply {
        graph.adjacencyList.keys.forEach { add(it) }
    }
    val previousNode = mutableMapOf<Node<T>, Edge<T>>()

    while (!pqNodes.isEmpty()) {
        val current = pqNodes.remove()
        val currentDistance = distances[current]!!

        if (current == dest) {
            break
        }

        graph.forEachEdgeOfNode(current) {
            val distance = it.weight
            val neighboursCurrentDistance = distances[it.nodeB]!!

            distances[it.nodeB] = min(neighboursCurrentDistance, currentDistance + distance)

            if (neighboursCurrentDistance != distances[it.nodeB]) {
                previousNode[it.nodeB] = it
                pqNodes.recalculatePosition(it.nodeB)
            }
        }

    }
    return backtrackEdgesFromPreviousNodeMapping(previousNode, dest)
}

private fun <T> backtrackEdgesFromPreviousNodeMapping(
    previousNode: MutableMap<Node<T>, Edge<T>>,
    dest: Node<T>
): MutableList<Edge<T>> {
    val result = mutableListOf<Edge<T>>()
    var currentEdge: Edge<T>? = previousNode[dest]
    while (currentEdge != null) {
        result.add(currentEdge)
        currentEdge = previousNode[currentEdge.nodeA]
    }
    return result
}
