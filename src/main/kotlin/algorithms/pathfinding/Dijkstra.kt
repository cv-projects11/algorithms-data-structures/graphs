package algorithms.pathfinding

import collections.Graph
import collections.Node
import collections.forEachEdgeOfNode
import java.lang.Double.min
import java.util.*

fun <T> dijkstra(graph: Graph<T>, src: Node<T>): Map<Node<T>, Double> {
    val distances: MutableMap<Node<T>, Double> = distanceInit(graph, src)
    val pqNodes = PriorityQueue<Node<T>>(graph.nodeCount, Comparator.comparing { distances[it]!! }).apply {
        graph.adjacencyList.keys.forEach { add(it) }
    }

    while (!pqNodes.isEmpty()) {
        val current = pqNodes.remove()
        val currentDistance = distances[current]!!

        graph.forEachEdgeOfNode(current) {
            val distance = it.weight
            val neighboursCurrentDistance = distances[it.nodeB]!!

            distances[it.nodeB] = min(neighboursCurrentDistance, currentDistance + distance)

            if (neighboursCurrentDistance != distances[it.nodeB]) {
                pqNodes.recalculatePosition(it.nodeB)
            }
        }
    }

    return distances
}



