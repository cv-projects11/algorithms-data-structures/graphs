package algorithms.pathfinding

import collections.Graph
import collections.Node
import java.util.*

fun <T> distanceInit(graph: Graph<T>, src: Node<T>) = mutableMapOf<Node<T>, Double>()
    .apply {
        graph.adjacencyList.keys.forEach {
            put(
                it, if (it == src) {
                    0.0
                } else {
                    Double.POSITIVE_INFINITY
                }
            )
        }
    }

fun <T> PriorityQueue<T>.recalculatePosition(value: T) {
    remove(value)
    add(value)
}
