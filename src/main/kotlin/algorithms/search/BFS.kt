package algorithms.search

import collections.Edge
import collections.Graph
import collections.Node

fun <T> bfs(graph: Graph<T>, src: Node<T>): BFSResult<T> {
    val bfsState = BFSState(src)
    bfsHelper(graph, src, bfsState, 0)

    return BFSResult(bfsState.nodeOrder, bfsState.distanceFromSource, bfsState.previousNode)
}

private fun <T> bfsHelper(
    graph: Graph<T>,
    current: Node<T>,
    bfsState: BFSState<T>,
    d: Int
) {
    val neighbours = graph[current].filter { bfsState.isNotVisited(it.nodeB) }
    neighbours.forEach { bfsState.visitNodeWithEdge(it, d + 1) }
    neighbours.forEach { bfsHelper(graph, it.nodeB, bfsState, d + 1) }
}

private class BFSState<T>(
    src: Node<T>,
    private var visitCounter: Int = 1,
    val nodeOrder: MutableMap<Node<T>, Int> = mutableMapOf(),
    val distanceFromSource: MutableMap<Node<T>, Int> = mutableMapOf(),
    val previousNode: MutableMap<Node<T>, Node<T>> = mutableMapOf(),
) {

    init {
        nodeOrder[src] = 0
        distanceFromSource[src] = 0
    }

    fun visitNodeWithEdge(edge: Edge<T>, d: Int) {
        nodeOrder[edge.nodeB] = visitCounter++
        distanceFromSource[edge.nodeB] = d
        previousNode[edge.nodeB] = edge.nodeA
    }

    fun isNotVisited(node: Node<T>) = !nodeOrder.containsKey(node)

}

data class BFSResult<T>(
    val nodeOrder: Map<Node<T>, Int>,
    val distanceFromSource: Map<Node<T>, Int>,
    val previousNode: Map<Node<T>, Node<T>>
)
