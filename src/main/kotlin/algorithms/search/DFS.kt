package algorithms.search

import collections.Edge
import collections.Graph
import collections.Node
import collections.forEachEdgeOfNode


fun <T> dfs(graph: Graph<T>, src: Node<T>): DFSResult<T> {
    val dfsState = DFSState(src)
    dfsHelper(graph, src, dfsState)

    return DFSResult(dfsState.depthOrder, dfsState.finishOrder, dfsState.previousNode)
}

private fun <T> dfsHelper(graph: Graph<T>, current: Node<T>, dfsState: DFSState<T>) {
    graph.forEachEdgeOfNode(current) {
        if (dfsState.isNotVisited(it.nodeB)) {
            dfsState.visitNodeWithEdge(it)
            dfsHelper(graph, it.nodeB, dfsState)
        }
    }
    dfsState.finishNode(current)
}

private class DFSState<T>(
    src: Node<T>,
    private var depthCounter: Int = 1,
    private var finishCounter: Int = 0,
    val depthOrder: MutableMap<Node<T>, Int> = mutableMapOf(),
    val finishOrder: MutableMap<Node<T>, Int> = mutableMapOf(),
    val previousNode: MutableMap<Node<T>, Node<T>> = mutableMapOf()
) {
    init {
        depthOrder[src] = 0
    }

    fun visitNodeWithEdge(edge: Edge<T>) {
        depthOrder[edge.nodeB] = depthCounter++
        previousNode[edge.nodeB] = edge.nodeA
    }

    fun finishNode(node: Node<T>) {
        finishOrder[node] = finishCounter++
    }

    fun isNotVisited(node: Node<T>) = !depthOrder.containsKey(node)
}


data class DFSResult<T>(
    val depthOrder: Map<Node<T>, Int>,
    val finishOrder: Map<Node<T>, Int>,
    val previousNode: Map<Node<T>, Node<T>>
)
