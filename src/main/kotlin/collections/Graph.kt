package collections

typealias Edge<T> = Graph.Edge<T>
typealias Node<T> = Graph.Node<T>

class Graph<T>(val adjacencyList: MultiValueMap<Node<T>, Edge<T>>) {

    operator fun get(label: T): Set<Edge<T>> = adjacencyList[Node(label)]
        ?: error("There is no node with label $label")

    operator fun get(node: Node<T>): Set<Edge<T>> {
        return this[node.label]
    }

    val nodeCount = adjacencyList.keys.size

    data class Node<T>(val label: T)
    data class Edge<T>(val nodeA: Node<T>, val nodeB: Node<T>, val weight: Double)

    class Builder<T> {
        private val nodes = mutableSetOf<Node<T>>()
        private val edges = mutableSetOf<Edge<T>>()

        fun node(label: T): Builder<T> {
            nodes.add(Node(label))
            return this
        }

        fun oneWayEdge(labelA: T, labelB: T, weight: Double): Builder<T> {
            edges.add(Edge(Node(labelA), Node(labelB), weight))
            return this
        }

        fun twoWayEdge(labelA: T, labelB: T, weight: Double): Builder<T> {
            oneWayEdge(labelA, labelB, weight)
            oneWayEdge(labelB, labelA, weight)
            return this
        }

        fun build(): Graph<T> {
            val builder = MultiValueMap.builder<Node<T>, Edge<T>>()
            nodes.forEach { builder._map[it] = mutableSetOf() }
            edges.forEach { builder._map[it.nodeB] = mutableSetOf() } // associate will create all nodeAs only
            return Graph(edges.associateMultiValueByTo(builder) { it.nodeA })
        }
    }

    companion object {
        fun <T> builder(): Builder<T> {
            return Builder()
        }
    }
}

inline fun <T> Graph<T>.forEachEdgeOfNode(node: Node<T>, block: (Edge<T>) -> Unit) {
    this.adjacencyList.forEachValueWithKey(node, block)
}

inline fun <T> Graph<T>.forEachEdge(block: (Edge<T>) -> Unit) {
    this.adjacencyList.forEachValueInEntries { _, value -> block(value) }
}

inline fun <T> Graph<T>.forEach(block: (Node<T>, Edge<T>) -> Unit) {
    this.adjacencyList.forEachValueInEntries { key, value -> block(key, value) }
}

