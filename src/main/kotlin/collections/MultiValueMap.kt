package collections

data class MultiValueMap<K, V>(val map: Map<K, Set<V>>) : Map<K, Set<V>> by map {

    class Builder<K, V>(val _map: MutableMap<K, MutableSet<V>> = mutableMapOf()) {
        fun put(key: K, value: V): Builder<K, V> {
            _map.compute(key) { _, v ->
                v?.apply { add(value) } ?: mutableSetOf(value)
            }
            return this
        }

        fun build(): MultiValueMap<K, V> = MultiValueMap(_map)
    }

    companion object {
        fun <K, V> builder(): Builder<K, V> {
            return Builder()
        }
    }
}

inline fun <K, V> MultiValueMap<K, V>.forEachValueWithKey(key: K, block: (value: V) -> Unit) {
    this[key]?.forEach(block)
}

inline fun <K, V> MultiValueMap<K, V>.forEachValueInEntries(block: (key: K, value: V) -> Unit) {
    this.map.entries.forEach { entry -> entry.value.forEach { block(entry.key, it) } }
}

inline fun <T, K> Iterable<T>.associateMultiValueBy(keySelector: (T) -> K): MultiValueMap<K, T> {
    val builder = MultiValueMap.builder<K, T>()
    return associateMultiValueByTo(builder, keySelector)
}

inline fun <T, K> Iterable<T>.associateMultiValueByTo(
    destination: MultiValueMap.Builder<K, T>,
    keySelector: (T) -> K
): MultiValueMap<K, T> {
    this.forEach { destination.put(keySelector(it), it) }
    return destination.build()
}


