package algorithms.challenges

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class StockProblemTest {


    @ParameterizedTest(name = "stock prices of {0} returns {1}")
    @MethodSource("testCases")
    fun findBestDaysToBuy(stockPrices: IntArray, expected: Pair<Int, Int>) {
        val result = findBestDaysToBuy(stockPrices)
        assertEquals(expected, result)
    }

    private fun testCases(): List<Arguments> {
        return listOf(
            Arguments.of(intArrayOf(5, 3, 6, 1, 5, 2), 3 to 4),
            Arguments.of(intArrayOf(1, 2, 3, 4, 5, 6), 0 to 5),
            Arguments.of(intArrayOf(7, 4, 3, 2, 1, 8), 4 to 5),
            Arguments.of(intArrayOf(2, 7, 3, 2, 1, 7), 4 to 5),
            Arguments.of(intArrayOf(3, 9, 3, 2, 1, 6), 0 to 1),
            Arguments.of(intArrayOf(3, 7, 2, 5, 8, 1), 2 to 4),
        )
    }

}
