package algorithms.pathfinding

import collections.Edge
import collections.Graph
import collections.Node
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class AStarTest {

    private val testGraph = Graph.builder<Char>()
        .oneWayEdge('s', 'a', 3.0)
        .oneWayEdge('s', 'f', 9.0)
        .oneWayEdge('s', 'e', 12.0)
        .oneWayEdge('a', 'b', 2.0)
        .oneWayEdge('a', 'f', 4.0)
        .oneWayEdge('b', 'c', 5.0)
        .oneWayEdge('b', 'd', 3.0)
        .oneWayEdge('b', 'f', 1.0)
        .oneWayEdge('c', 'd', 3.0)
        .oneWayEdge('e', 'd', 2.0)
        .oneWayEdge('f', 'c', 2.0)
        .oneWayEdge('f', 'e', 1.0)
        .build()

    @Test
    fun run() {
        val result = aStar(testGraph, Node('s'), Node('f'))
        val expected = listOf(
            Edge(Node('s'), Node('a'), 3.0),
            Edge(Node('a'), Node('b'), 2.0),
            Edge(Node('b'), Node('f'), 1.0)
        )

        assertEquals(expected, result.reversed())
    }
}
