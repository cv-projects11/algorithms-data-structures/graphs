package algorithms.pathfinding

import collections.Graph
import collections.Node
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class DijkstraTest {

    private val testGraph = Graph.builder<Char>()
        .oneWayEdge('s', 'a', 3.0)
        .oneWayEdge('s', 'f', 9.0)
        .oneWayEdge('s', 'e', 12.0)
        .oneWayEdge('a', 'b', 2.0)
        .oneWayEdge('a', 'f', 4.0)
        .oneWayEdge('b', 'c', 5.0)
        .oneWayEdge('b', 'd', 3.0)
        .oneWayEdge('b', 'f', 1.0)
        .oneWayEdge('c', 'd', 3.0)
        .oneWayEdge('e', 'd', 2.0)
        .oneWayEdge('f', 'c', 2.0)
        .oneWayEdge('f', 'e', 1.0)
        .build()

    @Test
    fun run() {
        val distances = dijkstra(testGraph, Node('s'))
        val expected = mapOf(
            's' to 0.0,
            'a' to 3.0,
            'b' to 5.0,
            'c' to 8.0,
            'd' to 8.0,
            'e' to 7.0,
            'f' to 6.0
        )

        assertEquals(expected, distances.mapKeys { it.key.label })
    }
}
