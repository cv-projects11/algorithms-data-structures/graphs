package algorithms.search

import collections.Graph
import collections.Node
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class BFSTest {

    private val testGraph = Graph.builder<Char>()
        .twoWayEdge('a', 'b', 0.0)
        .twoWayEdge('b', 'c', 0.0)
        .twoWayEdge('a', 'd', 0.0)
        .twoWayEdge('d', 'f', 0.0)
        .twoWayEdge('d', 'g', 0.0)
        .twoWayEdge('a', 'e', 0.0)
        .twoWayEdge('e', 'h', 0.0)
        .oneWayEdge('f', 'a', 0.0)
        .oneWayEdge('f', 'c', 0.0)
        .node('i')
        .build()

    // a <--> b <--> c
    // ^  <---      ^
    // |      ------|
    // | <-> d <--> f
    // |     <--> g
    // |
    // --> e -> h
    //
    // i
    @Test
    fun run() {
        val result = bfs(testGraph, Node('a'))
        val order = result.nodeOrder.entries.sortedBy { it.key.label }.map { it.value }
        val distances = result.distanceFromSource.entries.sortedBy { it.key.label }.map { it.value }
        val previous = result.previousNode.entries
            .sortedBy { it.key.label }.map { it.key.label to it.value.label }

        assertEquals(listOf(0, 1, 4, 2, 3, 5, 6, 7), order)
        assertEquals(listOf(0, 1, 2, 1, 1, 2, 2, 2), distances)
        assertEquals(
            listOf(
                'b' to 'a',
                'c' to 'b',
                'd' to 'a',
                'e' to 'a',
                'f' to 'd',
                'g' to 'd',
                'h' to 'e'
            ), previous
        )

    }
}
