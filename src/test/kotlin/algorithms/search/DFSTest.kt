package algorithms.search

import collections.Graph
import collections.Node
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class DFSTest {

    private val testGraph = Graph.builder<Int>()
        .twoWayEdge(1, 2, 0.0)
        .twoWayEdge(2, 4, 0.0)
        .twoWayEdge(1, 3, 0.0)
        .oneWayEdge(3, 4, 0.0)
        .twoWayEdge(1, 5, 0.0)
        .twoWayEdge(5, 6, 0.0)
        .twoWayEdge(5, 7, 0.0)
        .oneWayEdge(7, 1, 0.0)
        .node(8)
        .build()

    // 1 <--> 2 <--> 4
    //   <--> 3 -----^
    //   <--> 5 <--> 6
    //         <--> 7
    // ^           |
    // |-----------|
    //
    // 8
    @Test
    fun run() {
        val result = dfs(testGraph, Node(1))
        val order = result.depthOrder.entries.sortedBy { it.key.label }.map { it.value }
        val finishOrder = result.finishOrder.entries.sortedBy { it.key.label }.map { it.value }
        val previous = result.previousNode.entries
            .sortedBy { it.key.label }
            .map { it.key.label to it.value.label }
        //                  1  2  3  4  5  6  7
        assertEquals(listOf(0, 1, 3, 2, 4, 5, 6), order)
        assertEquals(listOf(6, 1, 2, 0, 5, 3, 4), finishOrder)
        assertEquals(
            listOf(
                2 to 1,
                3 to 1,
                4 to 2,
                5 to 1,
                6 to 5,
                7 to 5
            ), previous
        )
    }
}
