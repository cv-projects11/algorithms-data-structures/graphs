package collections

import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

internal class GraphTest {

    private val testGraph = Graph.builder<Int>()
        .twoWayEdge(1, 2, 3.0)
        .twoWayEdge(3, 2, 3.0)
        .twoWayEdge(4, 2, 3.0)
        .oneWayEdge(1, 5, 2.0)
        .node(6)
        .build()

    @Test
    fun create() {
        assertEquals(6, testGraph.nodeCount)
        assertEquals(
            setOf(
                Edge(Node(1), Node(5), 2.0),
                Edge(Node(1), Node(2), 3.0),
            ), testGraph[1]
        )
        assertEquals(
            setOf(
                Edge(Node(2), Node(1), 3.0),
                Edge(Node(2), Node(3), 3.0),
                Edge(Node(2), Node(4), 3.0),
            ), testGraph[2]
        )
        assertEquals(setOf(Edge(Node(3), Node(2), 3.0)), testGraph[3])
        assertEquals(setOf(Edge(Node(4), Node(2), 3.0)), testGraph[4])
        assertEquals(setOf(), testGraph[5])
        assertEquals(setOf(), testGraph[6])
    }

    @Test
    fun iterateEdgesByNodes() {
        testGraph.adjacencyList.keys.forEach {
            var edgeCount = 0
            testGraph.forEachEdgeOfNode(it) { edgeCount++ }
            assertEquals(testGraph[it.label].size, edgeCount)
        }
    }

    @Test
    fun iterateEdges() {
        var edgeCount = 0
        testGraph.forEachEdge { edgeCount++ }

        assertEquals(testGraph.adjacencyList.values.sumOf { it.size }, edgeCount)
    }

    @Test
    fun iterateGraph() {
        val expected = IntArray(6) { i -> testGraph[i + 1].size }

        val result = IntArray(6) { 0 }
        testGraph.forEach { node, _ ->
            result[node.label - 1]++
        }

        assertContentEquals(expected, result)
    }

}
