package collections

import kotlin.test.Test
import kotlin.test.assertEquals

internal class MultiValueMapTest {

    private val testMap = MultiValueMap.builder<Int, Int>()
        .put(1, 2)
        .put(1, 3)
        .put(2, 1)
        .put(3, 3)
        .put(3, 3)
        .put(3, 4)
        .put(3, 5)
        .build()

    @Test
    fun create() {
        assertEquals(setOf(2, 3), testMap[1])
        assertEquals(setOf(1), testMap[2])
        assertEquals(setOf(3, 4, 5), testMap[3])
    }

    @Test
    fun associateMultiValueBy() {
        val list = listOf(1 to 2, 2 to 4, 3 to 4, 3 to 5, 3 to 3)
        val map = list.associateMultiValueBy { it.first }
        val expected = MultiValueMap.builder<Int, Pair<Int, Int>>()
            .put(1, 1 to 2)
            .put(2, 2 to 4)
            .put(3, 3 to 3)
            .put(3, 3 to 4)
            .put(3, 3 to 5)
            .build()

        assertEquals(expected, map)
    }

}
